Make sure you've installed the following:

docker, docker-machine, docker-compose

Then

$ sudo docker-compose build
$ sudo docker-compose up
